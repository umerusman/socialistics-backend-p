from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import instaloader
from instagrapi import Client

app = FastAPI()

# Set up CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Replace with your specific origins if needed
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
async def read_root():
    return {"message": "Hello, World!"}

@app.get("/get_instagram_data")
async def get_instagram_data(post_url: str):
    try:
        # Initialize instaloader
        print(post_url)
        L = instaloader.Instaloader()

        # Load the post using its URL
        post = instaloader.Post.from_shortcode(L.context, post_url)

        cl = Client()
        cl.login("architecturalmodelmaker_", "Liquid098@")
        user_id = cl.user_id_from_username("architecturalmodelmaker_")
        medias = cl.user_medias(user_id, 20)
        media_pk = cl.media_pk_from_url(post_url)
        meta_Data = cl.insights_media(media_pk)
        comment_count = meta_Data["comment_count"]
        Likes_Count = meta_Data["like_count"]
        Save_Count = meta_Data["save_count"]

        # Create a list with a single dictionary
        data_list = [
            {
                "media_id": post.mediaid,
                "likes_count": post.likes,
                "comments_count": comment_count,
                "video_views_count": post.video_view_count,
                "save_count": Save_Count,
                "image_view_count": post.mediacount,
                "caption": post.caption,
                "owner_username": post.owner_username,
                "owner_id": post.owner_id,
                "taken_at_timestamp": post.date_utc.timestamp(),
                "is_video": post.is_video,
                "url": post.url,
            }
        ]

        return data_list

    except Exception as e:
        # Handle exceptions and return an HTTP response with an error message
        error_message = f"An error occurred: {str(e)}"
        raise HTTPException(status_code=500, detail=error_message)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=8000)
